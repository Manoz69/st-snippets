Sublim Text Snippets
======================

Here are some snippets that greatly increase my speed in Sublime Text (2 and 3).

## Installation

Copy the `Snippets` folder in your `/User` folder. You can find this folder by going to the "Preferences" > "browse packages ..." menu in ST2.

## How to..

### CSS

br>tab
```css
-webkit-border-radius: 5px;
border-radius: 5px;
```

box>tab
```css
box-shadow: 0 0 5px rgba(0,0,0,.4);
-webkit-box-shadow: 0 0 5px rgba(0,0,0,.4);
```

text>tab
```css
text-shadow: 0 1px 0 rgba(0,0,0,.6);
```

trans>tab
```css
-webkit-transition: color .3s ease-out;
-moz-transition: color .3s ease-out;
-ms-transition: color .3s ease-out;
-o-transition: color .3s ease-out;
transition: color .3s ease-out;
```

inl>tab
```css
display: inline-block;
vertical-align: top;
zoom: 1; /* Fix for IE7 */
*display: inline; /* Fix for IE7 */
```

upp>tab
```css
text-transform: uppercase;
```

### HTML

html>tab
```html
<html class="no-js" dir="ltr" lang="fr-FR">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>${1:My Title}</title>
        <meta name="description" content="${2:My description}">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/png" href="favicon.png">
        <link rel="author" href="https://plus.google.com/+XXXXXXXXXXXXX">

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">

        <meta property="og:title" content="">
        <meta property="og:url" content="">
        <meta property="og:type" content="">
        <meta property="og:image" content="">
        <meta property="og:description" content="">


        <!--[if lt IE 9]>
            <script src="dist/html5shiv.js"></script>
        <![endif]-->

    </head>
<body>
    $3
    <p>Hello world!</p>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.10.2.min.js"><\/script>')</script>

    <script src="js/main.js"></script>

    <!--
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>
    -->
</body>
</html>
```

inp>tab
```html
<label for="$1">$2</label>
<input type="text" name="$3" id="$4" />
```

lia>tab
```html
<li>
	<a title="$1" target="_blank" href="$2">$3</a>
</li>
```

a>tab
```html
<a title="$1" target="_blank" href="$2">$3</a>
```

aimg>tab
```html
<a title="$1" target="_blank" href="$2">
	<img src="$3" alt="$4" />
</a>
```

srt>tab
```html
<strong>$SELECTION</strong>
```
This one works differently. You can either type `str` in your html file as a classic snippet or add a shortcut to this snippet in your configuration file (Preferences> Key bindings> User). You just have to add the following lines:
```
{
    "keys": [ "ctrl+b"
    ],
    "command": "insert_snippet",
    "args": {
        "name": "Packages/User/Snippets/strong.sublime-snippet"
    }
},
```
Then, when you select an element in your html code, you just have to press "ctrl + b" (for example) to put your element <strong>. If you're on mac, replace "ctrl+b" by "super+b".

**WARNING** : Be careful to add a comma after each shortcut. Exemple in [this file](http://bit.ly/1kSGoIF). You can see the commas on lines 9 and 18.
